yarn add typescript -D
yarn tsc --init

yarn add ts-node-dev -D

yarn add express
yarn add @types/express -D

yarn add knex sqlite3      

* tem que criar os migrates na mão pq o knex ainda não cria eles com ts.
* tem que modificar o comando dos migrates no package.json para usar com ts.

yarn add cors
yarn add @types/cors -D
